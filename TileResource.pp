{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2024 P Blackman
License:    GPLv3+

Routine to report resources of given tile,
and its suitability for use as deadlands

***********************************************************}


procedure tMap.TileResource (const W,H : Integer;
                                out F,P,Tr : Integer;
                                out PosDeadLands : Boolean);
var T : tTerrain;
begin
    F := 0;
    P := 0;
    T := GetTerrain (W,H);
    Tr := 0; {Trade}

    case T of
    Coast:
        begin
            F  := 1;
            P  := 0;
            Tr := 3;
        end;
    Grass:
        begin
            F  := 4;
            P  := 0;
            Tr := 2;
        end;
    Desert:
        begin
            F  := 0;
            P  := 2;
            Tr := 2;
        end;
    Prairie:
        begin
            F  := 2;
            P  := 1;
            Tr := 2;
        end;
    Tundra:
        begin
            F  := 2;
            P  := 0;
            Tr := 2;
        end;
    Arctic:
        begin
            F  := 0;
            P  := 3;
            Tr := 0;
        end;
    Swamp:
        begin
            F  := 1;
            P  := 0;
            Tr := 2;
        end;
    Forest:
        begin
            F  := 1;
            P  := 2;
            Tr := 2;
        end;
    Hills:
        begin
            F  := 1;
            P  := 3;
            Tr := 1;
        end;
    Mountain:
        begin
            F  := 0;
            P  := 3;
            Tr := 1;
        end;
    otherwise
        // Ocean;
    end;

    { Bonus resources. Only first type usable in early game }
    if (GetBonus (W,H) = EarlyBonus) and (T <> Ocean) then
    case T of
        Grass:
            begin
                { plains }
                F := 3;
                P := 1;
            end;
        Coast:    F := 5;
        Desert:   F := 3;
        Prairie:  F := 4;
        Forest:   F := 3;
        Swamp:    P := 4;
        Mountain: P := 5;
        Tundra:   Tr:= 7;
        Arctic:
            begin
                F  := 3;
                Tr := 4;
            end;
        Hills : Tr := 4;
    end;

    { River Trade }
    Tr := Tr + Integer(Tiles[W,H].River);

    PosDeadLands := (getBonus (W,H) = Nothing) and (T in [Desert, Arctic, Mountain]) and not Tiles[W,H].River;
end;
