{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2023 P Blackman
License:    GPLv3+

Defaults if no Template selected

***********************************************************}


procedure TFrmInterface.DefaultLand;
begin
    RG_Size.ItemIndex      := 3;
    RG_Age.ItemIndex       := 1;
    RG_Mountains.ItemIndex := 2;
    RG_LandMass.ItemIndex  := 3;
end;

procedure TFrmInterface.DefaultRain;
begin
    RG_Rainequ.ItemIndex   := 1;
    RG_RainPoles.ItemIndex := 2;
    RG_Rivers.ItemIndex    := 1;

    // Advanced
    SpinWaveRain.Value := 12;
    SpinGainRain.Value := 60;
end;

procedure TFrmInterface.DefaultTemp;
begin
    RG_TempEqu.ItemIndex   := 1;
    RG_TempPoles.ItemIndex := 3;

    // Advanced
    SpinWaveTemp.Value := 12;
    SpinGainTemp.Value := 40;
end;

procedure TFrmInterface.MapDefaults;
begin
    DefaultLand;
    DefaultRain;
    DefaultTemp;
end;
