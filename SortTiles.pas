unit SortTiles;

interface uses MapTiles;

type tTileData =
    record
        Food,
        Prod,
        Trade : Integer;
        tt : tTerrain;
    end;
    tTileArray = array [0..20] of tTileData;

    procedure TileSort (Var TileArray : tTileArray);


implementation

// Sort by Food, Production then Trade
function Compare (const T1, T2 : tTileData) : Integer;
begin
    if T1.Food < T2.Food then
        Compare := -1
    else
    if T1.Food > T2.Food then
        Compare := 1
    else
    if T1.Prod < T2.Prod then
        Compare := -1
    else
    if T1.Prod > T2.Prod then
        Compare := 1
    else
    if T1.Trade < T2.Trade then
        Compare := -1
    else
    if T1.Trade > T2.Trade then
        Compare := 1
    else
        Compare := 0;
end;


procedure TileSort (Var TileArray : tTileArray);
Var J,K     : Integer;
    Found   : Boolean;
    Temp    : tTileData;

begin
    for K := 2 to 20 do  // City square 0, always included
        if compare (TileArray[K], TileArray[K-1]) > 0 then
        begin
            temp := TileArray[K];
            J := K;
            Found := False;

            while (J > 1) and not Found do
            iF Compare (TileArray[J-1], Temp) >= 0 then
                Found := True
            else
            begin
                TileArray[J] := TileArray[J-1];
                J := J-1;
            end;
            TileArray[J] := Temp;
        end;
end;

end.
