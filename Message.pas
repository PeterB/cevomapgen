unit Message;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  2024 P Blackman
License:    GPLv3+

General message routine.
(Writeln does not work in Windows, and
Dialogs.ShowMessage does not work in command lime programs)

***********************************************************}

interface
procedure ShowMessage (Msg : string);


implementation {$ifdef lcl} uses Dialogs; {$endif}


procedure ShowMessage (Msg : string);
begin
    {$ifdef lcl}
        Dialogs.ShowMessage(Msg);
    {$else}
        Writeln(Msg);
    {$endif}
end;

end.
