Unit Settings;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2024 P Blackman
License:    GPLv3+

Define SpinEdit values for RadioGroup indices

***********************************************************}

interface

type
    tSetting = (sLandMass, sResource, sMountains, sAge, sSize,
                sTempEqat, sTempPole, sRain, sRivers, sMapHeight, sMapWidth);

    tLandMassValues = array of Integer;
    tSettingsValues = array of tLandMassValues;

const
    SettingValues : tSettingsValues = (
                        (8,20,32,44,60,80),     // LandMass %
                        (-1,4,6,8),             // Resource %
                        (0,5,10,15,20),         // Mountains %
                        (2,3,4,5,6),            // Age
                        (8,12,16,22,28),        // Wave Size
                        (85,70,55,40,25),       // Temperature at Equator
                        (75,60,45,30,15),       // Temperature at Poles
                        (30,45,60,75,90),       // Rainfall
                        (2,4,6),                // Rivers

                        // Above values may be adjusted.
                        // Values below fixed in the game.

                        (46,52,60,70,82,90,96,120,132), // Map Height
                        (30,40,50,60,75,84,100,105,126) // Map Width
                        );


Function UpdateSetting (Idx : Integer; st : tSetting) : Integer;


implementation

Function UpdateSetting (Idx : Integer; st : tSetting) : Integer;
begin
    // Writeln ('Settings ', Idx, ' ', st);
    Assert ((ord(st) >= 0),                     'Setting type negative');
    Assert (ord(st) <= ord(high (tSetting)),    'Setting type out of range');

    Assert (Idx >= 0,                               'Setting index negative');
    Assert (Idx <= high (SettingValues[Ord(st)]),   'Setting index out of range');

    Result := SettingValues [Ord(st), Idx];
end;

end.
