{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2023 P Blackman
License:    GPLv3+

Load & Save Template files

***********************************************************}


procedure TFrmInterface.BtnLoadTemplateClick;
var MyFilename : string;
    IniFile: TIniFile;

begin
    OpenDialog2.InitialDir := GetAppConfigDir(False) + 'Templates';

    if OpenDialog2.Execute then
    begin
        MyFilename := OpenDialog2.filename;
        IniFile    := TINIFile.Create(myfilename);

        // Must load RadioGroup Indexes before the associated SpinEdit Values
        // Changing a Radio Group updates the SpinEdit

        RG_LandMass.ItemIndex   := IniFile.ReadInteger('LAND', 'RG_LandMass', 3);
        RG_Resource.ItemIndex   := IniFile.ReadInteger('LAND', 'RG_Resources',0);
        RG_Mountains.ItemIndex  := IniFile.ReadInteger('LAND', 'RG_Mountains',2);
        RG_Size.ItemIndex       := IniFile.ReadInteger('LAND', 'RG_Size',     3);
        RG_Age.ItemIndex        := IniFile.ReadInteger('LAND', 'RG_Age',      1);
        SE_OceanLevel.Value     := IniFile.ReadInteger('LAND', 'SE_LandMass', 44);
        SE_Resource.Value       := IniFile.ReadInteger('LAND', 'SE_Resources',-1);
        SE_MountainLevel.Value  := IniFile.ReadInteger('LAND', 'SE_Mountains',10);
        SpinWave.Value          := IniFile.ReadInteger('LAND', 'SE_Size',     22);
        SpinDim.Value           := IniFile.ReadInteger('LAND', 'SE_Age',      3);

        RG_RainPoles.ItemIndex  := IniFile.ReadInteger('RAIN', 'RG_RainPoles',2);
        RG_RainEqu.ItemIndex    := IniFile.ReadInteger('RAIN', 'RG_RainEquat',1);
        RG_Rivers.ItemIndex     := IniFile.ReadInteger('RAIN', 'RG_River'    ,1);
        SE_PolesRain.Value      := IniFile.ReadInteger('RAIN', 'SE_RainPoles',60);
        SE_RainEqu.Value        := IniFile.ReadInteger('RAIN', 'SE_RainEquat',45);
        SpinWaveRain.Value      := IniFile.ReadInteger('RAIN', 'SE_RainLen',  10);
        SpinGainRain.Value      := IniFile.ReadInteger('RAIN', 'SE_RainGain', 60);
        SpinWater.Value         := IniFile.ReadInteger('RAIN', 'SE_Water',    40) div 15;

        RG_TempEqu.ItemIndex    := IniFile.ReadInteger('TEMP', 'RG_TempEquat',1);
        RG_TempPoles.ItemIndex  := IniFile.ReadInteger('TEMP', 'RG_TempPoles',3);
        SE_PolesTemp.Value      := IniFile.ReadInteger('TEMP', 'SE_TempPoles',30);
        SE_EquTemp.Value        := IniFile.ReadInteger('TEMP', 'SE_TempEquat',70);
        SpinWaveTemp.Value      := IniFile.ReadInteger('TEMP', 'SE_TempLen',  12);
        SpinGainTemp.Value      := IniFile.ReadInteger('TEMP', 'SE_TempGain', 40);

        IniFile.Destroy;
    end;
end;


procedure TFrmInterface.SaveTemplate (MyFilePath : String);
var IniFile: TIniFile;

begin
    IniFile    := TINIFile.Create(MyFilePath);

    IniFile.WriteInteger('LAND', 'RG_LandMass', RG_LandMass.ItemIndex);
    IniFile.WriteInteger('LAND', 'RG_Resources',RG_Resource.ItemIndex);
    IniFile.WriteInteger('LAND', 'RG_Mountains',RG_Mountains.ItemIndex);
    IniFile.WriteInteger('LAND', 'RG_Size',     RG_Size.ItemIndex);
    IniFile.WriteInteger('LAND', 'RG_Age',      RG_Age.ItemIndex);
    IniFile.WriteInteger('LAND', 'SE_Resources',SE_Resource.Value);
    IniFile.WriteInteger('LAND', 'SE_LandMass', SE_OceanLevel.Value);
    IniFile.WriteInteger('LAND', 'SE_Mountains',SE_MountainLevel.Value);
    IniFile.WriteInteger('LAND', 'SE_Size',     SpinWave.Value);
    IniFile.WriteInteger('LAND', 'SE_Age',      SpinDim.Value);

    IniFile.WriteInteger('RAIN', 'RG_RainPoles',RG_RainPoles.ItemIndex);
    IniFile.WriteInteger('RAIN', 'RG_RainEquat',RG_RainEqu.ItemIndex);
    IniFile.WriteInteger('RAIN', 'RG_River',    RG_Rivers.ItemIndex);
    IniFile.WriteInteger('RAIN', 'SE_RainPoles',SE_PolesRain.Value);
    IniFile.WriteInteger('RAIN', 'SE_RainEquat',SE_RainEqu.Value);
    IniFile.WriteInteger('RAIN', 'SE_RainLen',  SpinWaveRain.Value);
    IniFile.WriteInteger('RAIN', 'SE_RainGain', SpinGainRain.Value);
    IniFile.WriteInteger('RAIN', 'SE_Water',    SpinWater.Value * 15);

    IniFile.WriteInteger('TEMP', 'RG_TempPoles',RG_TempPoles.ItemIndex);
    IniFile.WriteInteger('TEMP', 'RG_TempEquat',RG_TempEqu.ItemIndex);
    IniFile.WriteInteger('TEMP', 'SE_TempPoles',SE_PolesTemp.Value);
    IniFile.WriteInteger('TEMP', 'SE_TempEquat',SE_EquTemp.Value);
    IniFile.WriteInteger('TEMP', 'SE_TempLen',  SpinWaveTemp.Value);
    IniFile.WriteInteger('TEMP', 'SE_TempGain', SpinGainTemp.Value);

    IniFile.Destroy;
end;


procedure TFrmInterface.BtnSaveTemplateClick;
begin
    SaveDialog2.InitialDir := GetAppConfigDir(False) + 'Templates';

    if SaveDialog2.Execute then
        SaveTemplate (SaveDialog2.filename);
end;


procedure TFrmInterface.CopyTemplates;
var  DestDir, SrcDir: String;
     Src, Dst: TSearchRec;

begin
    Dst := default (TSearchRec);

    {$IFDEF WINDOWS}
        SrcDir  := 'Templates';
    {$ELSE}
        SrcDir  := '/usr/share/cevomapgen/Templates';
    {$ENDIF}

    DestDir := GetAppConfigDir(False) + 'Templates';

    if not DirectoryExists(DestDir) then
        ForceDirectories(DestDir);

    if FindFirst(SrcDir + DirectorySeparator + '*.INI', $21, Src) = 0 then
    repeat
        if (FindFirst(DestDir + DirectorySeparator + Src.Name, $21, Dst) <> 0) or (Dst.Time < Src.Time) then
            CopyFile ( SrcDir + DirectorySeparator + Src.Name, DestDir + DirectorySeparator + Src.Name, false);
        FindClose(Dst);
    until FindNext(Src) <> 0;

    FindClose(Src);
end;
