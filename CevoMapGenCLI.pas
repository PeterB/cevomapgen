program CevoMapGenCLI;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  2024 P Blackman
License:    GPLv3+

Command Line tool to generate a C-evo map from a template file

***********************************************************}

{$linklib c}

uses SysUtils, IniFiles, CevoMap, CevoMapFile, Settings, Rivers;

const
    Lacunarity  = 1.9;
    Bias        = 50;
    Gain        = 50;
    RainDim     = 10;
    TempDim     = 10;

var Width, Height, Ocean, RiversControl,
    Mountains, Resources, WaveLength, Dim,
    RainEqu, RainPoles, RainWave, RainGain,
    TempEqu, TempPoles, TempWave, TempGain  : Integer;

    TFileName,
    MFilePath : String;
    MyMap : TMap;


function GetParams : Boolean;
begin
    Result := True;
    if ParamCount >= 3 then
    begin
        try
            Width  := UpdateSetting(StrToInt(ParamStr(1)), sMapWidth);
            Height := UpdateSetting(StrToInt(ParamStr(1)), sMapHeight);

            if StrToInt(ParamStr(2)) <> 0 then
                RandSeed := StrToInt(ParamStr(2));

        except
            Writeln ('First two arguments must be integers');
            Writeln;
            Result := False;
        end;

        if Result then
        begin
            TFileName   := ParamStr(3);

            if NOT FileExists (TFileName + '.INI') then
            begin
                Result := False;
                Writeln ('Cannot find ' + TFileName + '.INI');
            end;

            if ParamCount < 4 then
                MFilePath := ''
            else
                MFilePath := ParamStr(4);
        end;
    end;

    if NOT Result then
    begin
        Writeln ('cevomapgencli expects at least three parameters;');
        Writeln ('1: Map Size Index, 0-8');
        Writeln ('2: Seed for PRNG, or zero for random');
        Writeln ('3: Template file name (Without the .INI extension)');
        Writeln ('4: Optional Path for output Map file (defaults to working directory)');
        Result := False;
    end;
end;


procedure LoadTemplate;
var IniFile    : TIniFile;
   MyFileName  : String;

begin
    MyFileName := TFileName + '.INI';
    IniFile    := TINIFile.Create(MyFileName);

    Ocean           := IniFile.ReadInteger('LAND', 'SE_LandMass', 44);
    Mountains       := IniFile.ReadInteger('LAND', 'SE_Mountains',10);
    WaveLength      := IniFile.ReadInteger('LAND', 'SE_Size',     22);
    Dim             := IniFile.ReadInteger('LAND', 'SE_Age',       3);
    Resources       := IniFile.ReadInteger('LAND', 'SEResource',  -1);

    RainPoles       := IniFile.ReadInteger('RAIN', 'SE_RainPoles',60);
    RainEqu         := IniFile.ReadInteger('RAIN', 'SE_RainEquat',45);
    RainWave        := IniFile.ReadInteger('RAIN', 'SE_RainLen',  10);
    RainGain        := IniFile.ReadInteger('RAIN', 'SE_RainGain', 60);
    RiversControl   := IniFile.ReadInteger('RAIN', 'SE_Water',     4);
    TempPoles       := IniFile.ReadInteger('TEMP', 'SE_TempPoles',30);
    TempEqu         := IniFile.ReadInteger('TEMP', 'SE_TempEquat',69);
    TempWave        := IniFile.ReadInteger('TEMP', 'SE_TempLen',  12);
    TempGain        := IniFile.ReadInteger('TEMP', 'SE_TempGain', 40);

    IniFile.Free;
end;


procedure BuildMap;
var MyRivers: tRivers;
begin
    MyMap := TMap.Create(Width, Height, Ocean, Mountains);

    with MyMap do
    begin
        GenMap(Altitude, WaveLength, Dim/10, Lacunarity, Bias, Bias, Gain, True);
        GenMap(Rainfall, RainWave, RainDim/20, Lacunarity, RainEqu, RainPoles, RainGain, False);
        GenMap(Temperature, TempWave, TempDim/20, Lacunarity, TempEqu, TempPoles, TempGain, False);

        ClearStats;
        AltitudeSort;
        AdjustSeaLevel;
        AdjustMountainLevel;

        MyRivers  := tRivers.Create(RiversControl);
        MyRivers.loaddata(MyMap);
        MyRivers.Generate;
        Myrivers.Free;

        MakeTerrain (Resources);
    end;
end;


procedure SaveMap;
var Mf: TMapFile;
    MFileName : String;
begin
    MFileName := MFilePath + ExtractFileName (TFileName) + '.cevomap';
    Mf        := TmapFile.Create;
    Mf.Height := MyMap.Height;
    Mf.Width  := MyMap.Width;
    Mf.SaveTileData(MyMap);
    Mf.SaveMFile(MFileName);
    Mf.Free;
    Writeln ('Created Cevo Map file ', MFileName);
end;


begin
    if GetParams then
    begin
        LoadTemplate;
        BuildMap;
        SaveMap;
    end;
end.
