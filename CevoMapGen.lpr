program CevoMapGen;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2023 P Blackman
License:    GPLv3+

Project file for CevoMapGen

***********************************************************}


uses
    Forms, Interfaces,
    CevoMapMain IN 'CevoMapMain.pas',
    ImageForm IN 'ImageForm.pas',
    LazLogger, LCLExceptionStackTrace;

{$R *.res}
{$hints off}

begin
    Application.Initialize;
    Application.Title := 'cevomapgen';
    Application.CreateForm(TFrmInterface, FrmInterface);
    Application.CreateForm(TFrmImage, FrmImage);

    // Load Map file if specified on command line
    FrmInterface.CheckifLoadNeeded;

    Application.Run;
end.
