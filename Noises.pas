
unit Noises;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2023 P Blackman
License:    GPLv3+

Routines for Variable Rate Noise,
distortions of Perlin Noise,
based on algorithms by F. Kenton Musgrave

***********************************************************}


interface uses Noise;

type
    tNoises = class(tNoise3)

    public
        function VRNoise3(point: vector; distortion: Double): Double;

    end;


implementation


// Function to implement "Variable Rate Noise"
function TNoises.VRNoise3(point: vector; distortion: Double): Double;
var Offset: Vector;
    distort: Double;

begin
    offset.x := point.x / 2 + 3.5; // misregister domain (arbitrary offsets)
    offset.y := point.y / 2 + 2.5;
    offset.z := point.z / 2 + 5.5;

    distort := 1 + distortion * Compute(offset);

    Point.X := Point.X * distort;
    Point.Y := Point.Y * distort;
    Point.Z := Point.Z * distort;

    Result := Compute(Point);
end;

end.
