# Makefile for cevomapgen
# Copyright : 2024 P Blackman
# License   : GPLv3+

.NOTPARALLEL:

HOME=tmphome

# Set bindir to games on Debian
bindir ?= bin
docdir ?= cevomapgen

all : cevomapgen cevomapcheck cevomapgencli

cevomapgen : fpc.cfg CevoMapGen.* *.pas *.pp *.lfm
	mkdir -p tmphome
	# LAZDIR not needed for Debian, but might be for others distros, ie Arch
	lazbuild --ws=qt5 -B $(LAZDIR) CevoMapGen.lpi

cevomapcheck: fpc.cfg CevoMapCheck.pas
	fpc -ocevomapcheck CevoMapCheck

cevomapgencli: fpc.cfg CevoMapGenCLI.pas
	fpc -ocevomapgencli CevoMapGenCLI

install:
	install -p -Dm 755 cevomapgen                 -t "$(DESTDIR)$(prefix)/$(bindir)"
	install -p -Dm 755 cevomapcheck               -t "$(DESTDIR)$(prefix)/$(bindir)"
	install -p -Dm 755 cevomapgencli              -t "$(DESTDIR)$(prefix)/$(bindir)"
	install -p -Dm 644 Desktop/cevomapgen.svg     -t "$(DESTDIR)$(prefix)/share/icons/hicolor/scalable/apps"
	install -p -Dm 644 Desktop/cevomapgen.desktop -t "$(DESTDIR)$(prefix)/share/applications"
	install -p -Dm 644 Templates/*.INI            -t "$(DESTDIR)$(prefix)/share/cevomapgen/Templates"
	install -p -Dm 644 Docs/*.html                -t "$(DESTDIR)$(prefix)/share/doc/$(docdir)"
	install -p -Dm 644 Docs/changelog             -t "$(DESTDIR)$(prefix)/share/doc/$(docdir)"
	install -p -Dm 644 ManPages/*.6               -t "$(DESTDIR)$(prefix)/share/man/man6"
	install -p -Dm 644 *.cevomapgen.*.xml         -t "$(DESTDIR)$(prefix)/share/metainfo"

clean:
	-rm -fr lib
	-rm -fr tmphome
	-rm *.o
	-rm *.ppu
	-rm *.res
