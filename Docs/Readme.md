Cevo.Map.Gen
============

ReadMe
======

  
  

##  Introduction

CevoMapGen is a map file generator for Cevo that facilitates harsher terrain
and climate.  
It gives greater control than the existing map generator within the game,  
and allows much faster generation of novel or extreme worlds than using the
map editor.
 
  

##  Features

  * Fast generation of random Cevo map files. 
  * Increased range and control over input parameters. 
  * More extreme terrain and climate possible. 
  * Sample world templates for instant use. 
  * Optional thumbnail view of map files. 
  * Estimate of city sites to indicate game length. 
  * City site count and thumbnail view possible with exiting maps. 
  * Support of larger maps for 'Distant Horizon' version of Cevo. 
  

##  Licence

CevoMapGen. Copyright @ 1999-2023 Peter Blackman. GPL3+

  

##  Notes

To hide the preview image, set the size to zero.  
You can save the preview image by right clicking on it.

  

##  Contact Information

Let me know if any problems are encountered with this program.  
Please specify the operating system and version number.  
Peter at PBlackman dot plus dot com

  

