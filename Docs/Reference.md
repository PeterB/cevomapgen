# Cevo.Map.Gen &nbsp; Reference

&nbsp;  
##  Introduction

CevoMapGen is a map file generator for Cevo that facilitates more varied and
harsher terrain and climate.
  
&nbsp;  
##  Getting Started

Hit the [New Land] button, and in a few seconds, a thumbnail image of a new Cevo map will appear.  
An analysis of the terrain as percentages of the whole is listed on the top right of the first tab page.  
Also shown is an estimate of the number or usable city sites on this map.  
The map can be saved via the [Save] button, and used in Cevo via it's 'Map' tab on the Start Window.  
The [Extra] tab page can be used to quickly generate typical worlds.  
Select one of the options here, and then hit [New Land].

&nbsp;  
##  Common Controls

### New Layout
Randomise and regenerate a new or different map.

### Same Layout
Retain the current land shape.  
Useful if you just wish to fine-tune the map
via the [Rain] and [Temp] pages.  
Use of [Same Layout] will clear the [Random] button if set on the [Land] page.

### Defaults      
Restore the settings on the page to the original defaults.

### Random
A random selection of the settings on this page will be used the
next time a map is generated.  
A random setting on the [Land] page is only applicable with use of [New Land]  

&nbsp;  
##  {Main}

###Map Size
This relates to the World Size option on the Cevo start window.  
Maps greater than 230%, 9,600 tiles, can only be used in the Distant Horizon variant of C-evo.

### Customise Map Size
This allows fine adjustment of the map size. The game
cannot use maps that exceed 16,632, 126 Width x 132 height.

### Map Files
Hit [SAVE] to preserve the current map as a file. A dialog opens to allow
section of the directory and file name.  
Previously used path and name are saved. Hit [LOAD] to view an existing map
file, and to show the terrain analysis and city estimates.

### Thumbnail View
The display can be sized to 1 to 4 pixels per square.  
Worlds can be scrolled horizontally to see the connection clearly.  
Rightclick on the thumbnail image to save it as a Jpeg.  
Selecting 0 hides the display, so that the layout of a new map remains a
surprise. The analysis can still be checked.

### Terrain Analysis (top right)
Shows each terrain type as a percentage of the total.  
The 'River' count is not exclusive to the other land types.

### No. Cities
Provides a rough estimate of the number of good city cites available on the map.

### Generate New Map
See 'Common Controls' (above)

&nbsp;  
##  {Land}

Note; the controls on this page indicate only a probability that a new map
with match the settings given.  
The nature of the map generator is that each map will differ, sometimes
considerably.  
Use the thumbnail view, or the analysis data on the [Control] page to check
the map before saving.

### Land Mass
This controls the amount of land overall compared to the amount
of Ocean.  

### Land Form
This represents the approximate size of islands, continents and lakes.  
However, most maps will contain a variety of shapes and sizes.  

### Mountains
This controls the percentage of land that becomes mountains.

### Age
Decreasing the planets age introduces more varied terrain
over shorter distances, and tends to break up long distance features.  
Increasing the age, simulates some weathering, and produces slightly smoother
terrain.  

&nbsp;  
##  {Rain}

Rainfall is randomly distributed with a bias toward higher altitude terrain.  
It then drains downhill, maybe forming a river that might drain into the sea
or a lake.

### Main Rainfall Controls
This controls the amount of rain, allowing for separate levels for the Poles and the Equator.

### Fine Tuning
'Distance' represents the approximate number of squares over which the rainfall pattern varies.  
'Variability' describes how much the random pattern of rain varies.   

### Rivers
This controls the probability of the formation of rivers.  

&nbsp;  
##  {Temp}

Temperature is randomly distributed with an initial bias toward higher
temperatures at the equator.

### Main Temperature Controls
This controls the temperature, allowing for separate levels for the Poles and the Equator.

### Fine Tuning
'Distance' represents the approximate number of squares over which the temperature pattern varies.  
'Variability' describes how much the random pattern of temperature varies.  

&nbsp;  
##  {Extra}

### Load & Save Template
Example worlds available for instant use.  
Selecting a template will set the controls on the preceding pages.  
These controls can be further adjusted if required.  
Curent settings can be kept by using 'Save Template'.  
On Linux, templates are stored in ~/.config/cevomapgen/Templates  
If a template file is deleted, CevoMapGen will recreate it in its original
form on the next start.

### Resources
Either use the Standard Pattern [-1]
(Needed for all versions of C-evo other than Distant Horizon)  
or define a percentage for randomly distributed resources.  

&nbsp;  
##  {About}

Please note the version number if you wish to contact the author about this program.

