unit MapTiles;

{***********************************************************

Project:    C-evo External Map Generator
Copyright:  1999-2023 P Blackman
License:    GPLv3+

Subroutines for Map Tiles that don't need to be in CevoMap.pas

***********************************************************}


interface

const
    lxMax = 126;
    lyMax = 132; // must be a mutiple of 4

type
    tTerrain =
        // Jungle is not used in map files
        // The game itself switches forest near the equator to jungle
        (Ocean, Coast, Grass, Desert, Prairie, Tundra, Arctic, Glacier = Arctic, Swamp,
        Jungle, Forest, Hills, Mountain);

    // Respresentation of one tile in the map, 4 bytes
    tTile =
    packed record
        Terrain,
        Improve,
        StartPos,
        Special:  Byte;
    end;


function Lowlands(R, T: Integer): tterrain;
function Steppe(R, T: Integer): tterrain;
function Highlands(R, T: Integer): tterrain;
function TerrainChar(T: tTerrain): Char;

implementation


function Lowlands(R, T: Integer): tterrain;
begin
    result := Grass;
    case R of
    0:
        case T of
            0: Result := Glacier;
            1: Result := Tundra;
            2: Result := Prairie;
            3: Result := Desert;
        end;
    1:
        case T of
            0: Result := Glacier;
            1: Result := Tundra;
            2: Result := Grass;
            3: Result := Desert;
        end;
    2:
        case T of
            0: Result := Swamp;
            1: Result := Grass;
            2: Result := Grass;
            3: Result := Forest;
        end;
    3:
        case T of
            0: Result := Swamp;
            1: Result := Swamp;
            2: Result := Forest;
            3: Result := Forest;
        end;
    end;
end;

function Steppe(R, T: Integer): tterrain;
begin
    result := Prairie;
    case R of
    0:
        case T of
            0: Result := Tundra;
            1: Result := Tundra;
            2: Result := Prairie;
            3: Result := Desert;
        end;
    1:
        case T of
            0: Result := Tundra;
            1: Result := Prairie;
            2: Result := Forest;
            3: Result := Desert;
        end;
    2:
        case T of
            0: Result := Glacier;
            1: Result := Forest;
            2: Result := Grass;
            3: Result := Prairie;
        end;
    3:
        case T of
            0: Result := Glacier;
            1: Result := Grass;
            2: Result := Forest;
            3: Result := Forest;
        end;
    end;
end;

function Highlands(R, T: Integer): tterrain;
begin
    result := Hills;
    case R of
    0:
        case T of
            0: Result := Tundra;
            1: Result := Hills;
            2: Result := Hills;
            3: Result := Desert;
        end;
    1:
        case T of
            0: Result := Tundra;
            1: Result := Hills;
            2: Result := Hills;
            3: Result := Prairie;
        end;
    2:
        case T of
            0: Result := Glacier;
            1: Result := Hills;
            2: Result := Hills;
            3: Result := Hills;
        end;
    3:
        case T of
            0: Result := Glacier;
            1: Result := Forest;
            2: Result := Hills;
            3: Result := Hills;
        end
    end;
end;


function TerrainChar(T: tTerrain): Char;
begin
    case T of
        Ocean: Result    := 'O';
        Coast: Result    := 'C';
        Grass: Result    := 'G';
        Desert: Result   := 'D';
        Prairie: Result  := 'R';
        Tundra: Result   := 'T';
        Arctic: Result   := 'A';
        Swamp: Result    := 'S';
        Forest: Result   := 'F';
        Hills: Result    := 'H';
        Mountain: Result := 'M';
    otherwise
        Result := '?';
    end;
end;

end.
